# htop
#
# Class to manage htop.
#
class htop(
  Variant[Enum['present','absent'], String] $ensure = present
) {
  package {
    'htop':
      ensure => $ensure;
  }
}
